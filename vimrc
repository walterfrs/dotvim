set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'tpope/vim-fugitive'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'scrooloose/nerdtree'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'
Plugin 'mileszs/ack.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'tpope/vim-surround'
Plugin 'majutsushi/tagbar'
Plugin 'easymotion/vim-easymotion'
Plugin 'tpope/vim-repeat'
Plugin 'junegunn/vim-easy-align'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'stephpy/vim-php-cs-fixer'
Plugin 'donnut/vim-php54-syntax'
Plugin 'mattn/emmet-vim'
Plugin 'benmills/vimux'

call vundle#end()
filetype plugin indent on
syntax enable

set showmode
set number
set showmatch
set incsearch
set hlsearch
set ignorecase
set smartcase
set laststatus=2
set wildmenu
set wildmode=list:longest,full
set whichwrap=b,s,h,l,<,>,[,]
set nowrap
set autoindent
set shiftwidth=4
set expandtab
set tabstop=4
set softtabstop=4

set term=screen-256color-bce
let g:solarized_termcolors=256
set t_Co=256
set background=light
colorscheme solarized


"# KEY MAPS ###################################################################
let mapleader=" " 
nnoremap <leader>s :wall<cr>
nnoremap j gj
nnoremap k gk

nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

"move to beginning/end of line
nnoremap B ^
nnoremap E $
" $/^ doesn't do anything
nnoremap $ <nop>
nnoremap ^ <nop>
" jk is escape
inoremap jk <esc>

" Airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme = 'solarized'

"NERDTree
map <leader>nn :NERDTreeToggle<cr>
map <leader>nb :NERDTreeFromBookmark 
map <leader>nf :NERDTreeFind<cr>

"ACK / The Silver Searcher
"aptitude install silversearcher-ag
if executable('ag')
    let g:ackprg = 'ag --vimgrep'
endif
cnoreabbrev Ack Ack!
nnoremap <Leader>a :Ack!<Space>

" EasyAlign
nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

" Tagbar
nmap <Leader>t :TagbarToggle<CR>

"PHP Code Fixer
let g:php_cs_fixer_level = "psr2"
let g:php_cs_fixer_config = "default"
let g:php_cs_fixer_path = "~/.vim/bin/php-cs-fixer.phar"
nnoremap <silent><leader>pcf :call PhpCsFixerFixFile()<CR>

" Utils
" Restaurar cursor en la ultima posicion
function! ResCur()
    if line("'\"") <= line("$")
        silent! normal! g`"
        return 1
    endif
endfunction

augroup resCur
    autocmd!
    autocmd BufWinEnter * call ResCur()
augroup END


" Repeat 
silent! call repeat#set("\<Plug>MyWonderfulMap", v:count)
